-module(pythag).
-export([pythag/1]).

pythag(N) ->
  Seq = lists:seq(1, N),
  [
    {A, B, C} ||
    A <- Seq, B <- Seq, C <- Seq,
    A + B + C < N,
    A*A + B*B =:= C*C
  ].
