-module(math_functions).
-export([split/1]).

split(L) -> split_acc(L, [], []).

split_acc([], Odd, Even) ->
  {Odd, Even};
split_acc(L, Odd, Even) ->
  [H|T] = L,
  if
    H rem 2 =:= 0 -> split_acc(T, Odd, [H|Even]);
    true -> split_acc(T, [H|Odd], Even)
  end.
