TARGET = $(addsuffix .beam, $(basename $(wildcard *.erl)))

build: $(TARGET)
.PHONY: all

clean:
	rm -f $(TARGET)
.PHONY: clean

%.beam: %.erl
	erlc $<
